## WPezToolbox - Bundle: Dev Themes

__ DEVT - A WPezToolbox bundle of developer-centic WordPress plugin tools for working on themes.__



> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Important

The plugins contained in WPezToolbox bundles are simply "you might consider these suggestions." They are not formal endorsements; there is not claim being made about their quality and/or functionality. Also, some links might be affiliate links. 

### Overview

See the README here: https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-loader


### This Bundle Includes

- https://wordpress.org/plugins/customizer-export-import

- https://wordpress.org/plugins/rtl-tester

- https://wordpress.org/plugins/regenerate-thumbnails

- https://wordpress.org/plugins/theme-inspector

- https://gitlab.com/WPezPlugins/wpez-theme-customize

- https://gitlab.com/WPezPlugins/wpez-theme-functions-php-for-todo

- https://gitlab.com/WPezPlugins/wpez-theme-unit-test



### Helpful Links

- https://gitlab.com/wpezsuite/WPezToolbox

- https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-loader


### TODO 



### CHANGE LOG

- v0.0.0 - 18 November 2019
   
   Proof of Concept

