<?php
/*
Plugin Name: WPezToolbox - Bundle: Dev Themes
Plugin URI: https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-dev-themes
Description: DEVT - A WPezToolbox bundle of developer-centic WordPress plugin tools for working on themes.
Version: 0.0.0
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: wpez_tbox
*/

namespace WPezToolboxBundleDevThemes;

add_filter( 'WPezToolboxLoader\plugins', __NAMESPACE__ . '\plugins' );


function plugins( $arr_in = [] ) {

	$str_bundle           = __( 'DEVT', 'wpez_tbox' );
	$str_prefix_separator = ' | ';
	$str_prefix           = $str_bundle . $str_prefix_separator;

	$arr = [

		'customizer-export-import' =>
			[
				'name'     => $str_prefix . 'Customizer Export/Import',
				'slug'     => 'customizer-export-import',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'The Beaver Builder Team',
						'url_by'  => 'https://www.wpbeaverbuilder.com/',
						'desc'    => 'The Customizer Export/Import plugin allows you to export or import your WordPress customizer settings from directly within the customizer interface!',
						'url_img' => 'https://ps.w.org/customizer-export-import/assets/icon-128x128.jpg?rev=1049984'
					],
					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/customizer-export-import/',
					]
				]
			],

		'rtl-tester' =>
			[
				'name'     => $str_prefix . 'RTL Tester',
				'slug'     => 'rtl-tester',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'Yoav Farhi, Automattic',
						'by_alt'  => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/yoavf/',
						'desc'    => 'This plugin adds a button to the admin bar that allow admins to switch the text direction of the site. It can be used to test WordPress themes and plugins with Right To Left (RTL) text direction.',
						'url_img' => 'https://ps.w.org/rtl-tester/assets/icon-128x128.png?rev=978435'
					],
					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/rtl-tester/',
						'url_site'   => 'https://yoav.blog/',
					]
				]
			],


		'regenerate-thumbnails' =>
			[
				'name'     => $str_prefix . 'Regenerate Thumbnails',
				'slug'     => 'regenerate-thumbnails',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'Alex Mills (Viper007Bond)',
						'by_alt'  => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/viper007bond/',
						'desc'    => 'Regenerate Thumbnails allows you to regenerate all thumbnail sizes for one or more images that have been uploaded to your Media Library.',
						'url_img' => 'https://ps.w.org/regenerate-thumbnails/assets/icon-128x128.png?rev=1753390'
					],
					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/regenerate-thumbnails/',
						'url_site'   => 'https://alex.blog/',
					]
				]
			],

		'theme-inspector' =>
			[
				'name'     => $str_prefix . 'Theme Inspector',
				'slug'     => 'theme-inspector',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'Melissa Cabral',
						'by_alt'  => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/melissa-cabral/',
						'desc'    => 'A simple, lightweight plugin that displays useful technical information on pages and posts to aid in developing WordPress themes. Provides quick access to things that are sometimes hidden, like post/page ID, slug, taxonomy terms, and post type slug. Theme Inspector tells you exactly what conditional tags are true on each view, and what template file loaded on each page view.',
						'url_img' => 'https://ps.w.org/theme-inspector/assets/icon-128x128.png?rev=1305896'
					],
					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/theme-inspector/',
						'url_site'   => 'http://melissacabral.com/',
					]
				]
			],

		'wpez-theme-customize' =>
			[
				'name'     => $str_prefix . 'WPezPlugins: WPezThemeCustomize',
				'slug'     => 'wpez-theme-customize',
				'source'   => 'https://gitlab.com/WPezPlugins/wpez-theme-customize/-/archive/master/wpez-theme-customize-master.zip',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Mark "Chief Alchemist" Simchock for Alchemy United',
						'url_by'  => 'https://AlchemyUnited.com',
						'desc'    => 'Maintain WordPress theme customizations via a stand-alone plugin. This is the boilerplate for such a plugin.',
						'url_img' => 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/5656210/wpezplugins.png?width=64',
					],

					'resources' => [
						'url_repo' => 'https://gitlab.com/WPezPlugins/wpez-theme-customize/',
						'url_site' => 'https://gitlab.com/WPezPlugins',
						'url_tw'   => 'https://twitter.com/WPezDeveloper'
					]
				]
			],

		'wpez-theme-functions-php-for-todo' =>
			[
				'name'     => $str_prefix . 'WPezPlugins: WPezThemeFunctionsPhp',
				'slug'     => 'wpez-theme-functions-php-for-todo',
				'source'   => 'https://gitlab.com/WPezPlugins/wpez-theme-functions-php-for-todo/-/archive/master/wpez-theme-functions-php-for-todo-master.zip',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Mark "Chief Alchemist" Simchock for Alchemy United',
						'url_by'  => 'https://AlchemyUnited.com',
						'desc'    => "Boilerplate for typical theme functions.php stuff decoupled into a free-standing plugin (so it's persistent and portable). For example, add_theme_support()'s 'editor-color-palette' is something that is often theme independent.",
						'url_img' => 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/14501385/wpezplugins.png?width=64',
					],

					'resources' => [
						'url_repo' => 'https://gitlab.com/WPezPlugins/wpez-theme-functions-php-for-todo',
						'url_site' => 'https://gitlab.com/WPezPlugins',
						'url_tw'   => 'https://twitter.com/WPezDeveloper'
					]
				]
			],


		'wpez-theme-unit-test' =>
			[
				'name'     => $str_prefix . 'WPezPlugins: Theme Unit Test',
				'slug'     => 'wpez-theme-unit-test',
				'source'   => 'https://gitlab.com/WPezPlugins/wpez-theme-unit-test/-/archive/master/wpez-theme-unit-test-master.zip',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Mark "Chief Alchemist" Simchock for Alchemy United',
						'url_by'  => 'https://AlchemyUnited.com',
						'desc'    => 'The WPTRT theme-unit-test.xml pre-downloaded, stashed in this plugin, and ready to be imported.',
						'url_img' => 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/15501229/wpezplugins.png?width=64',
					],

					'resources' => [
						'url_repo' => 'https://gitlab.com/WPezPlugins/wpez-theme-unit-test',
						'url_site' => 'https://gitlab.com/WPezPlugins',
						'url_tw'   => 'https://twitter.com/WPezDeveloper'
					]
				]
			],

	];

	$arr_mod = apply_filters( __NAMESPACE__ . '\plugins', $arr );

	if ( ! is_array( $arr_mod ) ) {
		$arr_mod = $arr;
	}

	$arr_new = array_values( $arr_mod );

	if ( is_array( $arr_in ) ) {

		return array_merge( $arr_in, $arr_new );
	}

	return $arr_new;
}